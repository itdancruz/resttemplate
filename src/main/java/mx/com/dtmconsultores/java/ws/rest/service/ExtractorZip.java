package mx.com.dtmconsultores.java.ws.rest.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ExtractorZip implements Extractor {

	@Override
	public List<File> extrae(File packaged) {
		return unZipIt(packaged.getAbsolutePath());
	}

	/**
	 * Unzip it
	 * 
	 * @param zipFile
	 *            input zip file
	 * @param output
	 *            zip file output folder
	 */
	public List<File> unZipIt(String zipFile) {
		List<File> result = null;
		byte[] buffer = new byte[1024];

		try {

			// create output directory is not exists
			File outputFolder = new File("/extractedFiles");
			if (!outputFolder.exists()) {
				outputFolder.mkdir();
			}

			// get the zip file content
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {
				if (null == result)
					result = new ArrayList<File>();
				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator + fileName);

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
				result.add(newFile);
			}

			zis.closeEntry();
			zis.close();

			return result;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
