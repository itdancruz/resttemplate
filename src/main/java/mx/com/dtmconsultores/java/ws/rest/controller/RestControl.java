package mx.com.dtmconsultores.java.ws.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.dtmconsultores.java.ws.rest.dto.StoreRequest;
import mx.com.dtmconsultores.java.ws.rest.dto.StoreResponse;
import mx.com.dtmconsultores.java.ws.rest.dto.User;
import mx.com.dtmconsultores.java.ws.rest.service.StoreService;

@RestController
public class RestControl {
	private static Logger LOG = LoggerFactory.getLogger(RestControl.class);
	@Autowired
	StoreService storeService;

	@RequestMapping(value = "/")
	public String home() {
		LOG.trace("Requested: /");
		return "NOT A VALIDA URL";
	}

	@RequestMapping(value = "/valida/{user}/{pwd}")
	public ResponseEntity<User> valida(@PathVariable("user") String user, @PathVariable("pwd") String pwd) {
		LOG.trace("Requested: /valida/{u}/{p}");
		User u = storeService.validaUsuario(user, pwd);
		if (null == u) {
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<User>(u, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/almacena", method = RequestMethod.POST)
	public ResponseEntity<StoreResponse> createEmployee(@RequestBody StoreRequest request) {
		LOG.trace("Requested: /almacena");
		StoreResponse response;
		try {
			response = storeService.almacena(request);
			Thread.sleep(5000);
			return new ResponseEntity<StoreResponse>(response, HttpStatus.CREATED);
		} catch (Exception e) {
			LOG.error("", e);
			return new ResponseEntity<StoreResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
