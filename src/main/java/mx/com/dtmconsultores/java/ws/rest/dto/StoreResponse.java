package mx.com.dtmconsultores.java.ws.rest.dto;

public class StoreResponse {

	private int idFormaIngreso;
	private String mensaje;

	public int getIdFormaIngreso() {
		return idFormaIngreso;
	}

	public void setIdFormaIngreso(int idFormaIngreso) {
		this.idFormaIngreso = idFormaIngreso;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
