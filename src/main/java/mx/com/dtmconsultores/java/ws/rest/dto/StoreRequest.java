package mx.com.dtmconsultores.java.ws.rest.dto;

public class StoreRequest {

	private String formaIngreso;
	private String nombreArchivo;
	private byte[] archivo;
	private String fechaIngreso;
	private String comentario;
	private int idUsuario;

	public String getFormaIngreso() {
		return formaIngreso;
	}

	public void setFormaIngreso(String formaIngreso) {
		this.formaIngreso = formaIngreso;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Override
	public String toString() {
		return "StoreRequest [formaIngreso=" + formaIngreso + ", nombreArchivo=" + nombreArchivo + ", archivo="
				+ "OMMITED, fechaIngreso=" + fechaIngreso + ", comentario=" + comentario + ", idUsuario=" + idUsuario
				+ "]";
	}
}
