package mx.com.dtmconsultores.java.ws.rest.service.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {
	private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

	public static File guardaByteArrayArchivo(byte[] file, String fileName) throws IOException {
		String path = "d:/tmp/";

		Calendar cal = Calendar.getInstance();
		int anio = cal.get(Calendar.YEAR);
		int mes = cal.get(Calendar.MONTH) + 1;
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		int hr = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int seg = cal.get(Calendar.SECOND);
		int ms = cal.get(Calendar.MILLISECOND);

		String mess = "0" + mes;
		mess = mess.substring(mess.length() - 2);

		String dias = "0" + dia;
		dias = dias.substring(dias.length() - 2);

		String hrs = "0" + hr;
		hrs = hrs.substring(hrs.length() - 2);

		String mins = "0" + min;
		mins = mins.substring(mins.length() - 2);

		String segs = "0" + seg;
		segs = segs.substring(segs.length() - 2);

		String mss = "000" + ms;
		mss = mss.substring(mss.length() - 4);

		String id = anio + mess + dias + "_" + hrs + mins + segs + mss;

		if (!new File(path).isAbsolute())
			path = "/" + path;

		path += "/" + id + "__" + fileName;
		File temp = new File(path);
		FileOutputStream stream = null;
		try {
			LOG.debug("Writting file " + temp.getAbsolutePath());
			stream = new FileOutputStream(temp);
			stream.write(file);

		} catch (Exception e) {
			LOG.error("Can not save the file", e);

		} finally {
			try {
				stream.close();
			} catch (IOException e) {

			}
		}

		return temp;

	}

	public static String getISODate() {

		Calendar cal = Calendar.getInstance();
		int anio = cal.get(Calendar.YEAR);
		int mes = cal.get(Calendar.MONTH) + 1;
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		int hr = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int seg = cal.get(Calendar.SECOND);
		int ms = cal.get(Calendar.MILLISECOND);

		String mess = "0" + mes;
		mess = mess.substring(mess.length() - 2);

		String dias = "0" + dia;
		dias = dias.substring(dias.length() - 2);

		String hrs = "0" + hr;
		hrs = hrs.substring(hrs.length() - 2);

		String mins = "0" + min;
		mins = mins.substring(mins.length() - 2);

		String segs = "0" + seg;
		segs = segs.substring(segs.length() - 2);

		String id = anio + mess + dias + " " + hrs + ":" + mins + ":" + segs + "." + ms;

		return id;

	}

}
