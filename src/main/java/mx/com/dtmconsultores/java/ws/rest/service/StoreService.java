package mx.com.dtmconsultores.java.ws.rest.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.dtmconsultores.java.ws.rest.controller.RestControl;
import mx.com.dtmconsultores.java.ws.rest.dao.AlmacenMapper;
import mx.com.dtmconsultores.java.ws.rest.dto.DoctoXml;
import mx.com.dtmconsultores.java.ws.rest.dto.StoreRequest;
import mx.com.dtmconsultores.java.ws.rest.dto.StoreResponse;
import mx.com.dtmconsultores.java.ws.rest.dto.User;
import mx.com.dtmconsultores.java.ws.rest.service.util.Utils;

@Service
public class StoreService {
	@Autowired
	AlmacenMapper dao;
	private static Logger LOG = LoggerFactory.getLogger(RestControl.class);

	public User validaUsuario(String user, String pwd) {
		User result = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user", user);
		params.put("pwd", pwd);
		Object res = dao.validaUsuario(params);
		try {
			if (null != res && res instanceof User) {
				result = (User) res;
			}
		} catch (Exception e) {
			LOG.error("No se pudo validar el usuario", e);
		}
		return result;
	}

	public StoreResponse almacena(StoreRequest request) throws IOException {
		LOG.info("Empezamos el proceso de almacenamiento para contenedor " + request);
		StoreResponse response = new StoreResponse();
		File tmp = Utils.guardaByteArrayArchivo(request.getArchivo(), request.getNombreArchivo());
		String tipo = tmp.getName().substring(tmp.getName().lastIndexOf(".") + 1);
		LOG.debug("Tipo de archivo detectado: " + tipo);
		ExtractorEnum tipoArchivo = ExtractorEnum.valueOf(tipo.toUpperCase());
		Extractor extractor = tipoArchivo.getImplementedClass();

		List<File> archivosDescomprimidos = extractor.extrae(tmp);
		if (null == archivosDescomprimidos || archivosDescomprimidos.size() < 1) {
			LOG.info("El contenedor " + request.getNombreArchivo() + " no contenia archivos.");
			response.setMensaje("El contenedor " + request.getNombreArchivo() + " no contenia archivos.");
		} else {
			LOG.debug("Almacenar forma de recepcion.");
			request.setFechaIngreso(Utils.getISODate());
			Map<String, Object> params = new HashMap<String, Object>();
			// params.put("archivo", request.getArchivo());
			params.put("coment", request.getComentario());
			params.put("fecha", request.getFechaIngreso());
			params.put("forma", request.getFormaIngreso());
			params.put("idUsuario", request.getIdUsuario());
			params.put("nombreArchivo", request.getNombreArchivo());
			params.put("idFormaIngreso", 0);
			dao.guardaFormaEntrada(params);
			int id = (int) params.get("idFormaIngreso");
			for (File xml : archivosDescomprimidos) {
				if (!xml.getName().toLowerCase().endsWith(".pdf")) {
					try {
						LOG.info("Procesando archivo XML: " + xml.getAbsolutePath());
						DoctoXml xmlBean = new DoctoXml();
						xmlBean.setIdFormaIngreso(id);
						String string = null;
						BufferedReader br = null;
						try {
							br = new BufferedReader(new InputStreamReader(new FileInputStream(xml), "utf-8"));
							String linea = "";
							string = "";
							while ((linea = br.readLine()) != null) {
								string += linea + System.lineSeparator();
							}
						} catch (Exception e) {
							throw e;
						} finally {
							br.close();
						}

						xmlBean.setXml(string);

						String nombre = xml.getAbsolutePath().substring(0, xml.getAbsolutePath().lastIndexOf("."));
						File pdfFile = new File(nombre + ".pdf");
						LOG.debug("Buscando PDF " + pdfFile.getAbsolutePath());
						Map<String, Object> paramsPDF = null;
						boolean flagPdf = false;
						if (pdfFile.exists()) {
							LOG.debug("PDF localizado para " + xml.getName());
							paramsPDF = new HashMap<String, Object>();
							byte[] pdfBA = Files.readAllBytes(pdfFile.toPath());
							paramsPDF.put("pdf", pdfBA);
							flagPdf = true;
						} else {
							LOG.debug("No se encontro PDF para " + xml.getName());
							params.put("coment", params.get("coment") + "[Sin archivo PDF encontrado en la carga]");
						}

						dao.guardaDocto(xmlBean);
						LOG.debug("Documento almacenado con ID " + xmlBean.getIdDocto());
						if (null != paramsPDF) {
							paramsPDF.put("idPdf", 0);
							paramsPDF.put("idDocto", xmlBean.getIdDocto());
							dao.guardaPDF(paramsPDF);
							LOG.debug("PDF almacenado con ID " + paramsPDF.get("idPdf"));
						}
						Map<String, Object> paramsDesc = new HashMap<String, Object>();
						paramsDesc.put("idDocto", xmlBean.getIdDocto());
						dao.descuartiza(paramsDesc);
						LOG.info(xml + "procesado OK" + (flagPdf ? " con " : " sin ") + "pdf");
					} catch (Exception e) {
						LOG.error("No se pudo almacenar " + xml.getAbsolutePath(), e);
					}
				}

			}
			response.setIdFormaIngreso(1);
			response.setMensaje("Archivo: " + tmp.getName());
		}

		return response;
	}

}
