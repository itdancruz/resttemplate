package mx.com.dtmconsultores.java.ws.rest.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import mx.com.dtmconsultores.java.ws.rest.dto.DoctoXml;
import mx.com.dtmconsultores.java.ws.rest.dto.User;

@Repository
public interface AlmacenMapper {

	public User validaUsuario(Map<String, Object> params);

	public int guardaFormaEntrada(Map<String, Object> params);

	public void guardaDocto(DoctoXml xmlBean);

	public void guardaPDF(Map<String, Object> paramsPDF);

	public void descuartiza(Map<String, Object> paramsDesc);
}
