package mx.com.dtmconsultores.java.ws.rest.service;

import java.io.File;
import java.util.List;

public interface Extractor {

	public List<File> extrae(File packaged);
}
