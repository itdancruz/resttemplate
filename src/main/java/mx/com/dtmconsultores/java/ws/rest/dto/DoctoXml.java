package mx.com.dtmconsultores.java.ws.rest.dto;

public class DoctoXml {
	private int idDocto;
	private String version;
	private String xml;
	private String comentario;
	private int idFormaIngreso;

	public int getIdDocto() {
		return idDocto;
	}

	public void setIdDocto(int idDocto) {
		this.idDocto = idDocto;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public int getIdFormaIngreso() {
		return idFormaIngreso;
	}

	public void setIdFormaIngreso(int idFormaIngreso) {
		this.idFormaIngreso = idFormaIngreso;
	}

}
