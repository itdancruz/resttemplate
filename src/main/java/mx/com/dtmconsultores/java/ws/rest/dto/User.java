package mx.com.dtmconsultores.java.ws.rest.dto;

public class User {
	private int idUsuario;
	private String usuario;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "User [idUsuario=" + idUsuario + ", usuario=" + usuario + "]";
	}

}
