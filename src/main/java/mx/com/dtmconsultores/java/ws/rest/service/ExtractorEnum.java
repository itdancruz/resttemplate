package mx.com.dtmconsultores.java.ws.rest.service;

public enum ExtractorEnum {
	ZIP("zip", new ExtractorZip()), 
	RAR("rar", new ExtractorRar());

	private String type;
	private Extractor classImplemented;

	private ExtractorEnum(String type, Extractor classGenera) {
		this.type = type;
		this.classImplemented = classGenera;
	}

	public String getType() {
		return this.type;
	}

	public Extractor getImplementedClass() {
		return classImplemented;
	}

}
